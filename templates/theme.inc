<?php

/**
 * @file
 * Theme functions for HFC BANE module.
 */

use Drupal\Core\Template\Attribute;

/**
 * Processes variables for hfc-bane-events-upcoming.html.twig template.
 */
function hfc_bane_preprocess_hfc_bane_events_upcoming(&$variables) {

  $events = $variables['element']['#events'];

  $items = [];
  foreach ($events as $event) {
    $attributes = [
      'class' => ['event-item', "event-{$event->nid}"],
    ];

    $items[] = [
      'event_date' => $event->event_date[0]->value,
      'link' => $event->link,
      'attributes' => new Attribute($attributes),
    ];
  }
  $variables['items'] = $items;
}

/**
 * Processes variables for hfc-bane-news-teaser.html.twig template.
 */
function hfc_bane_preprocess_hfc_bane_news_teaser(&$variables) {

  $entity = $variables['element']['#article'];

  $variables['nid'] = $entity->nid;
  $variables['title'] = $entity->title;
  $variables['body'] = !empty($entity->cut_line) ? $entity->cut_line : $entity->body;
  $variables['photo'] = $entity->photo_large;

  if (!empty($entity->alternative_link)) {
    $variables['attributes']['class'][] = 'hfc-bane-alt-link';
    $variables['node_url'] = $entity->alternative_link;
  }
  else {
    $variables['node_url'] = $entity->path;
  }

  $request_time = Drupal::time()->getRequestTime();
  $variables['event_date'] = NULL;
  if (!empty($entity->event_date)) {
    foreach ($entity->event_date as $event_date) {
      if ($event_date->value2 > $request_time) {
        $variables['event_date'] = $event_date->value;
        break;
      }
    }
  }
}

/**
 * Processes variables for hfc-bane-news-tiny.html.twig template.
 */
function hfc_bane_preprocess_hfc_bane_news_tiny(&$variables) {

  $entity = $variables['element']['#article'];

  $variables['title'] = $entity->link;
  $variables['thumbnail'] = $entity->photo_thumbnail;
  $variables['nid'] = $entity->nid;

  if (!empty($entity->alternative_link)) {
    $variables['attributes']['class'][] = 'hfc-bane-alt-link';
  }
}
