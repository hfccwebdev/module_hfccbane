<?php

namespace Drupal\hfc_bane\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Bulletin' block.
 *
 * @Block(
 *   id = "hfc_bane_bulletin_block",
 *   admin_label = @Translation("BANE Bulletin block"),
 * )
 */
class BaneBulletinBlock extends BaneBlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // We don't need these settings for this block,
    // but we need our parent block for dependency injection.
    unset($form['hfc_bane_block_settings']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $bulletins = $this->baneClient->getBulletin();
    if (!empty($bulletins)) {
      $bulletin = reset($bulletins);
      $output = [
        '#prefix' => '<div class = "notice-box-med-' . $bulletin->severity . '">',
        ['title' => ['#markup' => "<h2>{$bulletin->subject}</h2>"]],
        ['content' => (array) $bulletin->content],
        '#suffix' => '</div>',
      ];
    }
    $output['#cache'] = ['max-age' => 0];
    return $output;
  }

}
