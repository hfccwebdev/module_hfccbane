<?php

namespace Drupal\hfc_bane\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\hfc_bane\BaneClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base block implementation that this module's blocks will extend.
 *
 * This abstract class provides reusable elements.
 */
abstract class BaneBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Stores the BANE service.
   *
   * @var \Drupal\hfc_bane\BaneClientInterface
   */
  protected $baneClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('hfc_bane')
    );
  }

  /**
   * Constructs a new BANE block object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\hfc_bane\BaneClientInterface $baneClient
   *   The BANE Client service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    BaneClientInterface $baneClient
   ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->baneClient = $baneClient;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['hfc_bane_block_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Newsroom settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['hfc_bane_block_settings']['display_format'] = [
      '#type' => 'select',
      '#options' => $this->displayTypes(),
      '#title' => $this->t('Display format'),
      '#default_value' => $config['display_format'] ?? $this->defaultDisplay(),
      '#description' => $this->t('Select the display format to use.'),
      '#required' => TRUE,
    ];

    $form['hfc_bane_block_settings']['item_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Items to display'),
      '#default_value' => $config['item_count'] ?? NULL,
      '#description' => $this->t('Maximum number of items to display. Leave blank for unlimited.'),
      '#min' => 1,
      '#step' => 1,
    ];

    $form['hfc_bane_block_settings']['tag_filter'] = [
      '#type' => 'select',
      '#options' => ['' => '- none -'] + $this->baneClient->getTags(),
      '#title' => $this->t('Tag Filter'),
      '#default_value' => $config['tag_filter'] ?? NULL,
      '#description' => $this->t('Filter items based on the selected tag.'),
      '#required' => FALSE,
    ];

    $form['hfc_bane_block_settings']['more_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('More link'),
      '#default_value' => $config['more_link'] ?? NULL,
      '#description' => $this->t('Provide an optional path to display more items.'),
      '#weight' => 10,
    ];

    $form['hfc_bane_block_settings']['empty_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Empty results message'),
      '#default_value' => $config['empty_message'] ?? NULL,
      '#description' => $this->t('Message to display when no results are found. Leave blank to hide the block.'),
      '#weight' => 10,
    ];

    $form['hfc_bane_block_settings']['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Classes'),
      '#default_value' => $config['classes'] ?? NULL,
      '#description' => $this->t('Classes to add to the block display.'),
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues()['hfc_bane_block_settings'];
    $this->configuration['display_format'] = $values['display_format'];
    $this->configuration['item_count'] = $values['item_count'];
    $this->configuration['tag_filter'] = $values['tag_filter'];
    $this->configuration['more_link'] = $values['more_link'];
    $this->configuration['empty_message'] = $values['empty_message'];
    $this->configuration['classes'] = $values['classes'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($content = $this->getContent()) {
      return [
        'content' => [
          '#prefix' => '<div class="content">',
          'title_list' => $this->baneClient->displayTitleList($content),
          '#suffix' => '</div>',
        ],
        'more_link' => $this->moreLink(),
        '#cache' => ['max-age' => 0],
      ];
    }
    else {
      return $this->isEmpty();
    }
  }

  /**
   * Fetch content from the API.
   *
   * @return array
   *   The content to display.
   */
  protected function getContent(): array {

    $config = $this->getConfiguration();

    $resource = $this->getApiEndpoint();

    $filter_opts = [];

    if ($config['tag_filter']) {
      $filter_opts['tid'] = $config['tag_filter'];
    }

    $content = $this->baneClient->httpRequest($resource, $filter_opts);

    if (empty($content)) {
      return [];
    }

    if ($config['item_count']) {
      $content = array_slice($content, 0, $config['item_count']);
    }

    return array_map('\Drupal\hfc_bane\BaneClient::prepareContent', $content);
  }

  /**
   * Identity the appropriate newsfeed for the display.
   *
   * @return string
   *   The name of the API endpoint to query for content.
   */
  protected function getApiEndpoint() {
    return 'news-recent';
  }

  /**
   * The title for the "more items" link.
   *
   * @return string
   *   The link title.
   */
  protected function moreTitle() {
    return 'more news';
  }

  /**
   * Handle the empty content condition.
   *
   * @return array|null
   *   A renderable array of the empty message, if set.
   */
  protected function isEmpty() {
    $config = $this->getConfiguration();
    if (!empty($config['empty_message'])) {
      return [
        'content' => [
          '#type' => 'processed_text',
          '#text' => $config['empty_message'],
          '#filter' => 'plain_text',
        ],
        '#cache' => ['max-age' => 0],
      ];
    }
    else {
      return;
    }
  }

  /**
   * Generate a link to more items, if defined.
   *
   * @return array|null
   *   A renderable array of the "more" link.
   */
  protected function moreLink() {
    $config = $this->getConfiguration();
    if (!empty($config['more_link'])) {

      // @todo Add 'more_title' to configuration form.
      $title = $config['more_title'] ?? 'read more';
      $url = Url::fromUri($config['more_link']);

      $link = Link::fromTextAndUrl($title, $url);
      $link = $link->toRenderable();
      $link['#attributes'] = [
        'class' => ['hfc-button', 'hfc-button-primary'],
      ];

      return [
        '#prefix' => '<div class="links">',
        'link' => $link,
        '#suffix' => '</div>',
      ];
    }
    else {
      return;
    }
  }

  /**
   * Returns an array of allowed display types.
   */
  protected function displayTypes() {
    return $this->baneClient->getDisplayOpts('default');
  }

  /**
   * Sets the default display type.
   */
  protected function defaultDisplay() {
    return 'default';
  }

}
