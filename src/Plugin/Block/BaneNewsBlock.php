<?php

namespace Drupal\hfc_bane\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'News' block.
 *
 * @Block(
 *   id = "hfc_bane_news_block",
 *   admin_label = @Translation("BANE News block"),
 * )
 */
class BaneNewsBlock extends BaneBlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // @todo verify this is required or can we use tags instead?
    $form['hfc_bane_block_settings']['news_promote'] = [
      '#type' => 'select',
      '#options' => $this->baneClient->getPromoteOptions(),
      '#title' => $this->t('News Promotion'),
      '#default_value' => $config['news_promote'] ?? 'front',
      '#description' => $this->t('Select the promoted news to display.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues()['hfc_bane_block_settings'];
    $this->configuration['news_promote'] = $values['news_promote'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($content = $this->getContent()) {

      $display_format = $this->getConfiguration()['display_format'];

      if ($display_format == 'titles_only') {
        $output = $this->baneClient->displayTitleList($content);
      }
      else {
        $output = $this->baneClient->displayArticleList($content, $display_format);
      }

      return [
        'content' => [
          '#prefix' => '<div class="content">',
          'news' => $output,
          '#suffix' => '</div>',
        ],
        'more_link' => $this->moreLink(),
        '#cache' => ['max-age' => 0],
      ];
    }
    else {
      return $this->isEmpty();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiEndpoint() {
    $config = $this->getConfiguration();
    return 'news-' . $config['news_promote'];
  }

  /**
   * {@inheritdoc}
   */
  protected function displayTypes() {
    return $this->baneClient->getDisplayOpts('news');
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultDisplay() {
    return 'titles_only';
  }

}
