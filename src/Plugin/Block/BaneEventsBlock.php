<?php

namespace Drupal\hfc_bane\Plugin\Block;

/**
 * Provides an 'Events' block.
 *
 * @Block(
 *   id = "hfc_bane_events_block",
 *   admin_label = @Translation("BANE Events block"),
 * )
 */
class BaneEventsBlock extends BaneBlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($content = $this->getContent()) {

      $display_format = $this->getConfiguration()['display_format'];

      switch ($display_format) {
        case 'hfc_bane_events_upcoming':
          $content = [
            '#theme' => 'hfc_bane_events_upcoming',
            '#events' => $content,
          ];
          break;

        case 'hfc_bane_events_table':
          $content = $this->baneClient->displayUpcomingEventsTable($content);
          break;

        default:
          $content = $this->baneClient->displayTitleList($content);
          break;
      }

      return [
        'content' => [
          '#prefix' => '<div class="content">',
          'content' => $content,
          '#suffix' => '</div>',
        ],
        'more_link' => $this->moreLink(),
        '#cache' => ['max-age' => 0],
      ];
    }
    else {
      return $this->isEmpty();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiEndpoint() {
    return 'events-upcoming';
  }

  /**
   * {@inheritdoc}
   */
  protected function moreTitle() {
    return 'more events';
  }

  /**
   * {@inheritdoc}
   */
  protected function displayTypes() {
    return $this->baneClient->getDisplayOpts('event');
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultDisplay() {
    return 'hfc_bane_events_upcoming';
  }

}
