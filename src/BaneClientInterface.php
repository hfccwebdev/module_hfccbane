<?php

namespace Drupal\hfc_bane;

use Drupal\paragraphs\ParagraphInterface;

/**
 * The HFC BANE Client.
 */
interface BaneClientInterface {

  /**
   * Retrieve emergency alert bulletins.
   *
   * Do not cache results!
   *
   * @see yayb.module
   */
  public function getBulletin();

  /**
   * Retrieve a list of academic terms from the services host.
   */
  public function getTags(): array;

  /**
   * Retrieve a list of display options.
   *
   * @param string $feed_type
   *   The feed type to return.
   *
   * @return array
   *   An array of options.
   */
  public function getDisplayOpts($feed_type);

  /**
   * Retrieve a list of news promotion options.
   *
   * @return array
   *   An array of options.
   */
  public function getPromoteOptions();

  /**
   * Retrieve a URL from the services host.
   */
  public function httpRequest($resource, $filter_opts = []);

  /**
   * Prepare content for rendering.
   *
   * Passing through JSON turned the data from the newsroom API
   * into nested objects. We need to adjust this information
   * for use in render arrays.
   *
   * @param object $item
   *   The item to prepare.
   *
   * @return object
   *   The passed object, prepared for rendering.
   */
  public static function prepareContent($item);

  /**
   * Format content as an unordered list of title links.
   *
   * @param array $content
   *   The content to format.
   *
   * @return array
   *   A renderable array.
   */
  public function displayTitleList(array $content);

  /**
   * Format content as an array of renderable articles.
   *
   * @param array $content
   *   The content to format.
   * @param string $display_format
   *   The display mode for rendering.
   *
   * @return array
   *   A renderable array of content.
   */
  public function displayArticleList(array $content, string $display_format);

  /**
   * Format upcoming events table.
   *
   * @param array $content
   *   The content to format.
   *
   * @return array
   *   A renderable array.
   */
  public function displayUpcomingEventsTable(array $content);

  /**
   * Display BANE contents for a paragraph entity.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph containing parameters to load and render.
   *
   * @return array
   *   A renderable array.
   */
  public function displayParagraphContent(ParagraphInterface $paragraph);

}
