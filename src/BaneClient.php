<?php

namespace Drupal\hfc_bane;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\paragraphs\ParagraphInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

/**
 * The HFC BANE Client.
 */
class BaneClient implements BaneClientInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Configuration settings name.
   *
   * @var string
   */
  const SETTINGS = 'hfc_bane.client_settings';

  /**
   * Stores the GuzzleHttp Client service.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * Stores the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Stores the Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new BaneClient object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The Guzzle HTTP Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The Date Formatter service.
   */
  public function __construct(
    Client $http_client,
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $dateFormatter
  ) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * Retrieve emergency alert bulletins.
   *
   * Do not cache results!
   *
   * @see yayb.module
   */
  public function getBulletin() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      $output = $this->httpRequest('yayb');
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayOpts($feed_type) {
    switch ($feed_type) {
      case 'news':
        return [
          'titles_only' => $this->t('Titles only'),
          'hfc_bane_news_teaser' => $this->t('Teaser list'),
          'hfc_bane_news_tiny' => $this->t('Thumbnails and titles'),
        ];

      case 'event':
        return [
          'hfc_bane_events_upcoming' => $this->t('Upcoming events list with links'),
          'hfc_bane_events_table' => $this->t('Important dates table'),
        ];

      default:
        return ['default' => 'default'];

    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPromoteOptions() {
    return [
      'recent' => 'Recent News',
      'front' => 'Front Page',
      'public' => 'Public News',
      'student' => 'Student News',
      'employee' => 'Employee News',
    ];
  }

  /**
   * Retrieve a list of academic terms from the services host.
   */
  public function getTags(): array {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      if ($tags = $this->httpRequest('tags')) {
        $output = [];
        foreach ($tags as $tag) {
          $output[$tag->tid] = $tag->name;
        }
      }
      else {
        $this->messenger()->addWarning($this->t('Could not retrieve tags list.'));
        return [];
      }
    }
    return $output;
  }

  /**
   * Retrieve a URL from the services host.
   */
  public function httpRequest($resource, $filter_opts = []) {
    $config = $this->configFactory->get(static::SETTINGS);

    if (!$api_url = $config->get('hfc_bane_api_url', NULL)) {
      $this->messenger()->addWarning($this->t('Please configure the BANE services URL before using this function.'));
      return FALSE;
    }

    $request_url = "{$api_url}/{$resource}";

    try {
      $response = $this->httpClient->request('GET', $request_url, [
        RequestOptions::HTTP_ERRORS => FALSE,
        RequestOptions::QUERY => $filter_opts,
        RequestOptions::TIMEOUT => 10,
      ]);
    }
    catch (RequestException $e) {
      $this->messenger()->addWarning($this->t('An error occurred retrieving news. Please try again later.'));
      return [];
    }

    if ($response->getStatusCode() == 200) {
      $data = json_decode($response->getBody());
      return $data;
    }
    else {
      $this->messenger()->addWarning($this->t('An error occurred retrieving news. Please try again later.'));
      $this->getLogger('hfc_bane')->error('Error %code on BANE API %endpoint endpoint', [
        '%code' => $response->getStatusCode(),
        '%endpoint' => $resource,
      ]);
    }
    return [];
  }

  /**
   * Prepare content for rendering.
   *
   * Passing through JSON turned the data from the newsroom API
   * into nested objects. We need to adjust this information
   * for use in render arrays.
   */
  public static function prepareContent($item) {
    $item->title = Html::decodeEntities($item->title);
    $item->cut_line = !empty($item->cut_line)
      ? [
        '#type' => 'processed_text',
        '#text' => $item->cut_line->value,
        '#format' => $item->cut_line->format,
      ]
      : [];
    $item->body = !empty($item->body)
      ? [
        '#type' => 'processed_text',
        '#text' => $item->body->value,
        '#format' => $item->body->format,
      ]
      : [];
    $item->release_date = (array) $item->release_date;

    $link = !empty($item->alternative_link)
      ? $item->alternative_link
      : $item->path;

    $item->url = Url::fromUri($link);
    $item->link = Link::fromTextAndUrl($item->title, $item->url);

    return $item;
  }

  /**
   * {@inheritdoc}
   */
  public function displayTitleList(array $content) {
    $items = array_map(function ($item) {
      return $item->link;
    }, $content);

    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function displayArticleList(array $content, string $display_format) {

    return array_map(function ($item) use ($display_format) {
      return [
        '#theme' => $display_format,
        '#article' => $item,
      ];
    }, $content);
  }

  /**
   * {@inheritdoc}
   */
  public function displayUpcomingEventsTable(array $content) {
    $rows = array_map(function ($item) {
      $event_date = $this->dateFormatter->format(
        $item->event_date[0]->value,
        'custom',
        'n/j/Y'
      );
      return [
        $item->title,
        $event_date,
        ['data' => $item->cut_line],
      ];
    }, $content);
    return [
      '#theme' => 'table',
      '#header' => [
        $this->t('Event'),
        $this->t('Date'),
        $this->t('Notes'),
      ],
      '#rows' => $rows,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function displayParagraphContent(ParagraphInterface $paragraph) {

    $display_format = !empty($paragraph->field_bane_display_format->value)
      ? $paragraph->field_bane_display_format->value
      : 'default';

    $news_promote = !empty($paragraph->field_bane_news_promotion->value)
      ? $paragraph->field_bane_news_promotion->value
      : NULL;

    $item_count = !empty($paragraph->field_bane_item_count->value)
      ? $paragraph->field_bane_item_count->value
      : NULL;

    $tag_filter = !empty($paragraph->field_bane_tag_filter->value)
      ? $paragraph->field_bane_tag_filter->value
      : NULL;

    $empty_message = !empty($paragraph->field_bane_empty_message->value)
      ? $paragraph->field_bane_empty_message->value
      : NULL;

    $resource = (!empty($news_promote))
      ? "news-{$news_promote}"
      : 'events-upcoming';

    $filter_opts = [];

    if ($tag_filter) {
      $filter_opts['tid'] = $tag_filter;
    }

    $content = $this->httpRequest($resource, $filter_opts);

    if ($item_count) {
      $content = array_slice($content, 0, $item_count);
    }

    if (!empty($content)) {

      $content = array_map('self::prepareContent', $content);

      switch ($display_format) {
        case 'titles_only':
          $output = $this->displayTitleList($content);
          break;

        case 'hfc_bane_news_teaser':
        case 'hfc_bane_news_tiny':
          $output = $this->displayArticleList($content, $display_format);
          break;

        case 'hfc_bane_events_upcoming':
          $output = [
            '#theme' => 'hfc_bane_events_upcoming',
            '#events' => $content,
          ];
          break;

        case 'hfc_bane_events_table':
          $output = $this->displayUpcomingEventsTable($content);
          break;

        default:
          $output = $this->displayTitleList($content);

      }

      return [
        '#prefix' => '<div class="content">',
        'content' => $output,
        '#suffix' => '</div>',
        '#cache' => ['max-age' => 0],
        '#weight' => -99,
      ];
    }
    elseif (!empty($empty_message)) {
      return [
        'content' => [
          '#type' => 'processed_text',
          '#text' => $empty_message,
          '#filter' => 'plain_text',
        ],
        '#cache' => ['max-age' => 0],
        '#weight' => -99,
      ];
    }
    else {
      return;
    }
  }

}
