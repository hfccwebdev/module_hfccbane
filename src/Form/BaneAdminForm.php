<?php

namespace Drupal\hfc_bane\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the BANE Admin Settings Form.
 */
class BaneAdminForm extends ConfigFormBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'hfc_bane.client_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_bane_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['message'] = [
      '#type' => 'item',
      '#markup' => $this->t('Administration page for HFC News Services.'),
    ];
    $api_url = $config->get('hfc_bane_api_url', NULL);

    $form['hfc_bane_api_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['hfc_bane_api_settings']['hfc_bane_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HFC News API address'),
      '#default_value' => $api_url,
      '#description' => $this->t('Enter the Base URL path to the services API. (ex: http://www.example.com/api/1.0)'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configurations and save.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('hfc_bane_api_url', $form_state->getValue('hfc_bane_api_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
